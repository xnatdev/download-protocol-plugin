# XNAT 1.7 Download Protocol Plugin #

This is the XNAT 1.7 Download Protocol enabler plugin. Its purpose is to overwrite the existing Imaging Data Download page to add the ability to generate a custom XML Manifest file that can be opened by the XNAT Desktop Client, which will read the manifest and download files to a specified location on your desktop. The XNAT Desktop Client can be downloaded at https://download.xnat.org. 

This plugin will not be updated, as these functions will be incorporated directly into core XNAT as of version 1.7.5. This is strictly for use in earlier versions of XNAT or builds that forked off from earlier versions of XNAT. 

# Version 1.0 Release Notes #

* Overwrites the default XDATScreen_download_sessions.vm file with a new version that adds the Manifest download link, and adds a plugin-specific version of the project download handler script. 
* Extends URL.js with a method that creates xnat:// URLs that can be handled by the XNAT Download Client 

# Building & Installing #

To build the XNAT 1.7 Download Protocol plugin:

1. If you haven't already, clone this repository and cd to the newly cloned folder.
1. Build the plugin: `./gradlew jar` (on Windows, you can use the batch file: `gradlew.bat jar`). This should build the plugin in the file **build/libs/xnat-download-protocol-1.0.0.jar**.
1. Copy the plugin jar to your plugins folder: `cp build/libs/xnat-download-protocol-1.0.0.jar /data/xnat/home/plugins`
1. Restart Tomcat and your plugin will become active in XNAT. 


