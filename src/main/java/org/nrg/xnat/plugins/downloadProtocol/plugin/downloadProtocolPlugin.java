/*
 * xnat-download-protocol: org.nrg.xnat.plugins.downloadProtocol.plugin.downloadProtocolPlugin
 * XNAT http://www.xnat.org
 * Copyright (c) 2018, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.plugins.downloadProtocol.plugin;

import org.nrg.framework.annotations.XnatPlugin;

@XnatPlugin(value = "downloadProtocolPlugin", name = "XNAT 1.7 Download Protocol Enabler Plugin", description = "Adds the xnat:// manifest download protocol to the XNAT Download page, in support of the XNAT Desktop Client.")

public class downloadProtocolPlugin {
}
